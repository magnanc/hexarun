﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public GameObject Bob;
	private float diff1;
	private float diff2;

	// Use this for initialization
	void Start () {
		diff1 = this.transform.position.z - Bob.transform.position.z;
		diff2 = this.transform.position.y - Bob.transform.position.y;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 temp = this.transform.position;
		temp.x = Bob.transform.position.x;
		temp.y = Bob.transform.position.y + diff2;
		temp.z = Bob.transform.position.z + diff1;
		this.transform.position = temp;

	}
}
